import { useState } from "react";

const Content = () => {
  const [text, setText] = useState('');
  const [givenText, setGivenText] = useState('');

  const handleInputChange = (event) => {
    setText(event.target.value);
  };

  const handleButtonClick = () => {
    setGivenText(text);
    setText('');
  };

  return (
    <div className='container my-5'>
              <div className='p-5 text-center bg-body-tertiary rounded-3'>
      <textarea className="form-control mb-3" value={text} onChange={handleInputChange}></textarea>
      <div className="d-inline-flex gap-2 mb-2">
        <button className="d-inline-flex align-items-center btn btn-primary btn-lg px-4 rounded-pill" onClick={handleButtonClick}>Update</button>
      </div>
      <p className="col-lg-8 mx-auto fs-5 text-muted">
        Entered Text: {givenText}
      </p>
    </div>
    </div>
  );
};

export default Content;
