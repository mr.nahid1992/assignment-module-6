import Content from "./components/contentSection/Content"
import Footer from "./components/footer/Footer"
import Header from "./components/header/Header"


function App() {
 

  return (
    <>
      <Header title="Module 6 Assignment"/>
          
      <Content/> 
            
      <Footer/>

    </>
   
  )
}

export default App
